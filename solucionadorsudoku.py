#Patricio Norambuena
#Para resolver el sudoku se utilizara un metodo de backtracking
#grid = g
g = [[5,3,0,0,7,0,0,0,0],
        [6,0,0,1,9,5,0,0,0],
        [0,9,8,0,0,0,0,6,0],
        [8,0,0,0,6,0,0,0,3],
        [4,0,0,8,0,3,0,0,1],
        [7,0,0,0,2,0,0,0,6],
        [0,6,0,0,0,0,2,8,0],
        [0,0,0,4,1,9,0,0,5],
        [0,0,0,0,8,0,0,7,9]]
#Definimos el puzzle
R=9 
#Usaremos el grid como una matriz de 9x9 con submatrizes de 3x3 para representar la tabla del sudoku
#definimos las funciones correspondientes para que el grid actue como puzzle para ello asignamos "n" para columnas y filas
def puzzle(q):
        for i in range(R):
                for j in range(R):
                        print(q[i][j],end= "  " )
                print()
def solve(g,r,c,n):
        for x in range(9):
                if g[r][x] == n:
                        return False
        for x in range(9):
                if g[x][c] == n:
                        return False
#                        
        startr = r - r % 3
        startc = c - c % 3
        for i in range(3):
                for j in range(3):
                        if g[i+startr][j+startc] == n:
                                return False
        return True
def sudoku(g, r, c):
        if (r == R-1 and c ==R):
                return True
        if c == R:
                r += 1
                c  = 0
        if g[r][c]>0:
                return sudoku(g, r, c+1)
        for n in range(1,R+1,1):
                if solve(g, r, c ,n):
                        g[r][c]=n
                        if sudoku(g, r, c+1):
                                return True
                g[r][c]=0
        return False
if (sudoku(g, 0, 0)):
        puzzle(g)
else:
        print("No posee solucion")        
